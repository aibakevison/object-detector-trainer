import os
import pathlib
import cv2

PREFIX_IMAGE = '2019'

def resize_image(image, width=None, height=None, inter=cv2.INTER_AREA):
    dim = None
    (h, w) = image.shape[:2]

    if width is None and height is None:
        return image

    if width is None:
        r = height / float(h)
        dim = (int(w * r), height)
    else:
        r = width / float(w)
        dim = (width, int(h * r))

    resized = cv2.resize(image, dim, interpolation=inter)

    return resized

SOURCE_PATH = 'C:\\dev\\ai\\asyncml\\dataset\\source'
IMAGESETS_PATH = 'C:\\dev\\ai\\asyncml\\dataset'
classes = ["patternless", "lilywhite"]

count = 1

imagesets_file = open(os.path.join(IMAGESETS_PATH, "train.txt"), "w")

for cls in classes:
    image_path = os.path.join(SOURCE_PATH, cls)
    files = os.listdir(image_path)

    for name in files:
        ext = pathlib.PurePosixPath(name).suffix
        new_name = name.replace(name, PREFIX_IMAGE + "_{0:06d}".format(count)) + ext
        imagesets_file.writelines(new_name+"\n")
        os.rename(os.path.join(image_path, name), os.path.join(image_path, new_name))
        count += 1

        imgdata = cv2.imread(os.path.join(image_path, new_name))

        if imgdata.shape[1] >= 608:
            resize_data = resize_image(imgdata, width=608)
            cv2.imwrite(os.path.join(image_path, new_name), resize_data)


imagesets_file.close()
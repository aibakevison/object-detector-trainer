#!/bin/bash
./darknet/darknet detector train cg.data yolov3-cg.cfg darknet/darknet53.conv.74
mv /workspace/output/yolov3-cg_final.weights /workspace/output/final
aws s3 cp /workspace/output/final s3://aibakevision-object-detection/darknet/weights --recursive
